import React, {
    Component
} from 'react';

//We need to export just the SearchBar component
class SearchBar extends Component {
    constructor(props){
        super(props);

        //Initializing state.
        this.state = { term: ''};
    }
    render() {
        //Here we use an arrow function as a handler. We can also use seperate function to handle the event.
        //To change the state we use setState(object).
        return (
        <div className="search-bar">
        <input 
        value={this.state.term}
        onChange= { (event)=> this.onInputChange(event.target.value) } />
        </div>
        );
    }

    //Event Handler can also be given like this.
    onInputChange(term){
        //changes state
        this.setState({term});

        //fires the callback function
        this.props.onSearchTermChange(term);
    }
}

//Export the correct component.
export default SearchBar;
