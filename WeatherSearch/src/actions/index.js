import axios from 'axios';
import rootReducer from "../reducers";

const API_KEY = '6195690dae137c8be72506289d18bf23';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city){
    const url = `${ROOT_URL}&q=${city},us`;
    const request = axios.get(url); //axios will return a promise object

    //returning the action
    return {
        type: FETCH_WEATHER,
        // request is a promise object which is returned as payload
        payload: request
    };
}