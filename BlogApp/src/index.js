import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import promise from 'redux-promise';

import reducers from './reducers';
/**
 * browser history deals with entire route
 * hashHistory deals with a portion of url after a hash symbol
 * eg: www.google.com/#
 * memoryHistory dont use urls
 */
import { Router, browserHistory } from 'react-router';
import routes from './routes'; 

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <Router history={browserHistory} routes={routes} />
  </Provider>
  , document.querySelector('.container'));
