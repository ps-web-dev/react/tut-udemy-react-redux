import React, { Component } from 'react';

export default class App extends Component {
  render() {
    return (
      <div>
        {/* To display nested routes */}
        {this.props.children}
      </div>
    );
  }
}
