import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import {reduxForm} from 'redux-form';
import {createPost} from '../actions/index';
import {Link} from 'react-router';

class PostsNew extends Component {
    static contextTypes = {
        //react interprets this object whenever an instance of PostsNew is created.
        //we need access on specific property named 'router' within the context.
        //react will search all of this component's parents until it finds a component that has a piece of context called router
        router: PropTypes.object
    };
    onSubmit(props){
        //props is the properties from the form as it was binded
        //this method returns a promise
        this.props.createPost(props)
            .then(()=>{
                //blog post have been created (promise is complete). navigate the user to the index
                //we navigate the user by calling this.context.push with the new path to navigate to.
                //react router is accessible to all components of the app via context property.
                this.context.router.push('/');
            });
    }
    render() {
        // handleSumbit will notify when the user submits the form.
        // fields maps the name of inputs
        const {fields: {title, categories, content}, handleSubmit} = this.props;
        // redux-form will have a config object for each input field and we need to map it to that field
        return (
            <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <h3>Create a new post</h3>
                {/*invalid is triggered if the field value is not valid */}
                <div className={`form-group ${title.touched && title.invalid ? 'has-danger' : ''}`}>
                    <label>Title</label>
                    {/* to assign the config object created by redux-form we give {...field_name} */}
                    <input type="text" className="form-control" {...title}/>
                    <div className="text-help">
                        {/*touched triggers only after the element is touched atleast once */}
                        {title.touched ? title.error : ''}
                    </div>
                </div>
                <div className={`form-group ${categories.touched && categories.invalid ? 'has-danger' : ''}`}>
                    <label>Categories</label>
                    <input type="text" className="form-control" {...categories}/>
                    <div className="text-help">
                        {categories.touched ? categories.error : ''}
                    </div>
                </div>
                <div className={`form-group ${content.touched && content.invalid ? 'has-danger' : ''}`}>
                    <label>Content</label>
                    <textarea className="form-control" {...content}/>
                    <div className="text-help">
                        {content.touched ? content.error : ''}
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">Add</button>
                <Link to="/" className="btn btn-danger">Cancel</Link>
            </form>
        );
    }
}

//for form validation : if form is not valid submission won't work
function validate(values) {
    const errors = {};

    //title is the name of the field
    if(!values.title){
        errors.title = 'Enter a username';
    }
    if(!values.categories){
        errors.categories = 'Enter a category';
    }
    if(!values.content){
        errors.content = 'Enter content';
    }

    return errors;
}

/**reduxForm will inject the helpers for form handling into props.
 * connect(mapStateToProps, mapDispatchToProps)
 * reduxForm(formConfig, mapStateToProps, mapDispatchToProps)
 * */
export default reduxForm({
    form: 'PostsNewForm',
    fields: ['title', 'categories', 'content'],
    validate
}, null, {createPost})(PostsNew);


// when user types in something then it is recorded in application state in background.
// state === {
//     form: {
//         PostsNewForm: {
//             title:'...',
//             categories:'...',
//             content: '...'
//         }
//     }
// }


