import React, {Component} from 'react';

//imports connect function from react-redux
import {connect} from 'react-redux';

import {selectBook} from '../actions/index';

import {bindActionCreators} from 'redux';

class BookList extends Component{
    renderList(){
        return this.props.books.map((book) => {
            // When the element is clicked call the action creator
            return (
                 <li 
                 key={book.title} 
                 onClick={()=> this.props.selectBook(book)}
                 className="list-group-item">
                {book.title}
                 </li> 
            );
        });
    }
    render(){
        return(
            <ul className="list-group col-sm-4">
                {this.renderList()}
            </ul>
        );
    }
}

//function takes app state as arguement. Whenever our app state changes, the container will automatically re-render
function mapStateToProps(state){
    //Whatever is returned is shown as props inside of BookList
    return {
        books: state.books
    };
}

// Anything returned from this function will end up as props on the BookList container.
function mapDispatchToProps(dispatch){
    //whenever selectBook is called result should be passed to all of our reducers. 
    //when ever selectBook action creator is called the result is given to dispatch and it will deliver it to all the reducers. 
    return bindActionCreators({selectBook: selectBook}, dispatch);
}

//connect function takes in a function and a component and produces a container.
//Container is aware of state that contained by redux
//Whenever the app state changes, the object in state function will be assigned as props of the component
// Promote BookList from a component to a container - it needs to know about the new dispatch method, selectBook and make it available as a prop.
export default connect(mapStateToProps, mapDispatchToProps)(BookList);