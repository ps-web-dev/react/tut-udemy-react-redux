export function selectBook(book) {
    //selectBook is and action creator and it needs to return an action, an object with a type property
    return {
        // an action always contains a type and the payload or any number of properties that are optional.
        type: 'BOOK_SELECTED',
        payload: book
    };
}