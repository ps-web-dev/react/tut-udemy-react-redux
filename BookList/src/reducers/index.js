import { combineReducers } from 'redux';
import BookReducer from './reducer_books';
import ActiveBook from './reducer_active_book';

//combined reducer adds keys and values to the global application state. It combines all the reducers together.
const rootReducer = combineReducers({
  // For each key we create a reducer. And it will manage a piece of the application state.
  books: BookReducer,
  activeBook: ActiveBook
});

export default rootReducer;
