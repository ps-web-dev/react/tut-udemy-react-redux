export default function() {
    return [
        {title: "Javascript", pages: 101},
        {title: "CSS", pages: 99},
        {title: "HTML", pages: 65},
        {title: "Laravel", pages: 500}
    ];
}